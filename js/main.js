/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global $, window */


if (!Element.prototype.remove) {
  Element.prototype.remove = function remove() {
    if (this.parentNode) {
      this.parentNode.removeChild(this);
    }
  };
}

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        // xhrFields: {withCredentials: true},
        url: '/server/php/',
        method: 'POST',
        maxChunkSize: 100000000, // 100 MB
        maxFileSize: 1000000000, // 1G
        sequentialUploads: true,
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(avi|mkv|mp4|jpe?g|png|mov|m4v|flv|wmv|pdf|doc|docx|bmp|tif)$/i,
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    $('#fileupload').bind('fileuploadcompleted', function (e, data) {

        if (data.result.files[0].error) {
            console.log('trouble!')
            return;
        } else {

            let verificationInput = document.getElementById('la-forme__lа-verification')

            if (verificationInput.nextSibling.textContent === 'Необходимо загрузить файл(ы)') {
                verificationInput.nextSibling.remove()
            }

            verificationInput.value = document.querySelectorAll('.upl1')[0].value

            Array.from(document.querySelectorAll('.delete-upl1')).forEach(delBtn => {
                delBtn.addEventListener('click', () => {
                    if (document.querySelectorAll('.upl1').length == 1) {
                        verificationInput.value = ''
                    }
                })
            })
            console.log('not trouble!')
        }


    })

    // $('#fileupload').fileupload('send', {files: filesList}).complete(function (result, textStatus, jqXHR) {
    //     console.log('pls delete it')
    // });

    // if (window.location.hostname === 'blueimp.github.io') {
    //     // Demo settings:
    //     $('#fileupload').fileupload('option', {
    //         url: '//jquery-file-upload.appspot.com/',
    //         // Enable image resizing, except for Android and Opera,
    //         // which actually support image resizing, but fail to
    //         // send Blob objects via XHR requests:
    //         disableImageResize: /Android(?!.*Chrome)|Opera/
    //             .test(window.navigator.userAgent),
    //         maxFileSize: 999000,
    //         acceptFileTypes: /(\.|\/)(gif|jpe?g|png|mkv|mp4)$/i
    //     });
    //     // Upload server status check for browsers with CORS support:
    //     if ($.support.cors) {
    //         $.ajax({
    //             url: '//jquery-file-upload.appspot.com/',
    //             type: 'HEAD'
    //         }).fail(function () {
    //             $('<div class="alert alert-danger"/>')
    //                 .text('Upload server currently unavailable - ' +
    //                         new Date())
    //                 .appendTo('#fileupload');
    //         });
    //     }
    // } else {
        // Load existing files:
        // $('#fileupload').addClass('fileupload-processing');
        // $.ajax({
        //     // Uncomment the following to send cross-domain cookies:
        //     //xhrFields: {withCredentials: true},
        //     url: $('#fileupload').fileupload('option', 'url'),
        //     dataType: 'json',
        //     context: $('#fileupload')[0]
        // }).always(function () {
        //     $(this).removeClass('fileupload-processing');
        // }).done(function (result) {
        //     $(this).fileupload('option', 'done')
        //         .call(this, $.Event('done'), {result: result});
        // });
    // }

});
