import ContentPopup from './ContentPopup'

const pathsB1 = {
    img: 'https://b1.m24.ru/c/',
    video: 'https://b1.m24.ru/c/'
}

function DOMIsReady() {

    if (document.getElementById('contentus')) {

        const contentus = document.getElementById('contentus')

        contentus.innerHTML = 'Загрузка списка участников...'

        fetch('http://pismapobedy.ru/heroes/frontend/web/site/get')
            .then(res => res.json())
            .then(data => {
                const participantsList = data
                return participantsList;
            })
            .then(participantsList => {
                // Draw participants list
                setTimeout(() => {
                    contentus.innerHTML = ''
                    participantsList.forEach(participantItem => {

                        let participantCard = document.createElement('div')
                        participantCard.className = ''
                        participantCard.innerHTML = `
                        <div
                            data-fancybox="images"
                            data-src='${pathsB1.img + (participantItem.img[0] ? participantItem.img[0] : 1214310)}.jpg'
                        >
                            <span>${participantItem.fio}</span>
                        </div>`

                        contentus.appendChild(participantCard)
                    })
                }, 1000)
                return participantsList;
            })
            .then(participantsList => {
                // Init detail popup
                const popup = new ContentPopup(participantsList, '', 'http://pismapobedy.ru/')

                Array.from(document.querySelectorAll('.test-content-btn')).forEach(participantNode => {

                    participantNode.addEventListener('click', () => {
                        console.log('click')
                        popup.initPopup()
                            .then(() => popup.showContentFromData(participantsList))
                    })
                })
                // document.querySelector('.test-content-btn').addEventListener('click', () => {
                //     console.log('click')
                //     popup.initPopup()
                // })
            })
    }

}

document.addEventListener('DOMContentLoaded', DOMIsReady);
