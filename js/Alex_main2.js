console.log('Alex_main2.js init')
// import 'babel-polyfill';

// Validate ListInfoForm

document.forms.ListInfoForm.querySelector('#la-forme__la-desc').value = ' '

let birthMask = new IMask(
    document.getElementById('la-forme__la-date'), {
        mask: Date,
        min: new Date(1900, 0, 1),
        max: new Date(2020, 0, 1)
})

let phoneMask = new IMask(
    document.getElementById('la-forme__le-telephone'), {
    mask: '+{7}(000)000-00-00'
})

// or new window.JustValidate(form)
new JustValidate('.la-forme', {
    rules: {
        name: {
            required: true,
            minLength: 3,
            maxLength: 100,
            strength: {
                custom: '^[ А-ЯЁа-яё-]+$'
            }
        },
        birth: {
            required: false,
            minLength: 10,
            strength: {
                custom: '^[0-9.\s]+$'
            }
        },
        city: {
            required: true,
            minLength: 2,
            maxLength: 50,
            strength: {
                custom: '^[-А-ЯЁа-яё\s]+$'
            }
        },
        email: {
            required: true,
            email: true,
        },
        phonez: {
            required: true,
            minLength: 16
        },
        description: {
            required: true,
            maxLength: 9000
        },
        check_1: {
            required: true,
        },
        check_2: {
            required: true,
        },
        verification_upload: {
            required: true,
        }
    },
    messages: {
        name: {
            required: 'Поле обязательное для заполнения',
            minLength: 'Минимум :value буквы',
            maxLength: 'Максимум :value букв',
            strength: 'Требуется ввести только буквы русского алфавита'
        },
        birth: {
            minLength: 'Введите полную дату рождения',
            strength: 'Введите дату рождения в формате dd.mm.yyyy'
        },
        city: {
            required: 'Поле обязательное для заполнения',
            minLength: 'Минимум :value буквы',
            maxLength: 'Максимум :value букв',
            strength: 'Требуется ввести только буквы русского алфавита'
        },
        email: {
            required: 'Поле обязательное для заполнения',
            email: 'Введите валидный email (пример: example@email.ru)'
        },
        phonez: {
            required: 'Поле обязательное для заполнения',
            minLength: 'Требуется ввести полный номер',
        },
        description: {
            required: 'Поле обязательное для заполнения',
            maxLength: 'Максимум :value символов',
        },
        check_1: {
            required: 'Подтвердите своё согласие с пунктом №1',
        },
        check_2: {
            required: 'Подтвердите своё согласие с пунктом №2',
        },
        verification_upload: 'Необходимо загрузить файл(ы)'
    },
    colorWrong: "#fc7e67",
    submitHandler: function (form, values, ajax) {
      console.log('coooool!')

      // Ajax Post ListInfoForm

      let ListInfoForm = $('#la-forme').serialize()

      $.ajax({
          url: 'http://pismapobedy.ru/heroes/frontend/web/site/form-data',
          type: 'post',
          async: true,
          data: ListInfoForm,
          beforeSend: () => {
              console.log('lets go ajax')
              // Before Send popup init
              document.getElementById('bs-overlay').style.visibility = 'visible'
          },
          success: result => {
              console.log('success ajax')
              console.log(result)
          },
          error: result => {
              console.log('error ajax')
              console.log(JSON.parse(result.responseText).errors)
          },
          complete: result => {
              console.log('complete ajax')

              console.log(result.responseJSON.success)

              if (result.responseJSON.success) {
                  setTimeout(() => {
                      // Success popup init
                      document.getElementById('sc-overlay').style.visibility = 'visible'
                      // Cleaning of a form
                      document.getElementById('la-forme__lа-verification').value = ''
                      document.forms.ListInfoForm.reset()
                      // Cleaning of a loader
                      let filesTBody = document.querySelector('.files')
                      while (filesTBody.firstChild) {
                          filesTBody.removeChild(filesTBody.firstChild);
                      }
                  }, 1250)
              } else {
                  console.log('request ajax error')
              }

          },
          callback: response => {
              console.log(response)
          }
      })
    }
})

document.getElementById('sv-overlay-ok').addEventListener('click', function() {
    document.getElementById('sc-overlay').style.visibility = ''
    document.getElementById('bs-overlay').style.visibility = ''
})
