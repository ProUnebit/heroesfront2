$(function () {

    if (!Element.prototype.remove) {
      Element.prototype.remove = function remove() {
        if (this.parentNode) {
          this.parentNode.removeChild(this);
        }
      };
    }

    'use strict';

    $('#fileupload').fileupload({
        url: '/heroes/frontend/web/server/php/',
        method: 'POST',
        maxChunkSize: 100000000, // 100 MB
        maxFileSize: 1000000000, // 1G
        sequentialUploads: true,
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(avi|mkv|mp4|jpe?g|png|mov|m4v|flv|wmv|pdf|doc|docx|bmp|tif)$/i
    });

    $('#fileupload').bind('fileuploadcompleted', function (e, data) {

        if (data.result.files[0].error) {
            console.log('trouble!')
            return;
        } else {

            let verificationInput = document.getElementById('la-forme__lа-verification')

            if (verificationInput.nextSibling.textContent === 'Необходимо загрузить файл(ы)') {
                verificationInput.nextSibling.remove()
            }

            verificationInput.value = document.querySelectorAll('.upl1')[0].value

            Array.from(document.querySelectorAll('.delete-upl1')).forEach(delBtn => {
                delBtn.addEventListener('click', () => {
                    if (document.querySelectorAll('.upl1').length == 1) {
                        verificationInput.value = ''
                    }
                })
            })
            console.log('not trouble!')
        }
    })
});
