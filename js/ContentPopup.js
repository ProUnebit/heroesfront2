// import mediaelementplayer from 'mediaelement'
// import jQuery from 'jquery'
// import $ from 'jquery'
import './jquery-global.js';
import fancybox from '@fancyapps/fancybox'
import '../css/jquery.fancybox.min.css'
import { TweenMax, TimelineMax } from "gsap/TweenMax"
import 'sharer.js'


export default class ContentPopup {

    constructor(participantsList, sectionID, siteUrl) {
        this.participantsList = participantsList
        this.sectionID = sectionID
        this.siteUrl = siteUrl
        this.siteHash
    }

    initPopup(participantsList) {

        console.log('init popup')

        console.log(this.participantsList)

        this.siteHash = window.location.hash

        return Promise.resolve(this.sectionID + ' init')
    }

    // async getData() {
    //
    //     try {
    //
    //         let res = await fetch('http://pismapobedy.ru/heroes/frontend/web/site/get', {
    //             method: 'GET',
    //             cache: 'no-cache'
    //         })
    //
    //         if (res.status === 200) {
    //             let data = await res.json()
    //             // for class value (constructor)
    //             this.data = data
    //             // return data for new call function arg (next "then")
    //             return data;
    //         // if status is not 200
    //         } else { throw new Error(res.status) }
    //
    //     } catch (err) {
    //         // if fetch->return failed (incorrectly path or something)
    //         throw new Error('Не удалось получить данные участника. Ошибка: ' + err.message)
    //     }
    // }

    showContentFromData(participantsList) {

        console.log(participantsList)

        $('[data-fancybox]').fancybox({
            protect: true,
            buttons : [
              'slideShow',
              'share',
              'zoom',
              'fullScreen',
              'close'
            ]
        });
    }
}
